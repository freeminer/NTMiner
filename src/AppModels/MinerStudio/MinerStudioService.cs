﻿using NTMiner.Core.MinerClient;
using NTMiner.Core.MinerServer;
using NTMiner.MinerStudio.Impl;
using System;
using System.Collections.Generic;

namespace NTMiner.MinerStudio {
    public class MinerStudioService : IMinerStudioService {
        public static MinerStudioService Instance { get; private set; } = new MinerStudioService();

        public readonly LocalMinerStudioService LocalMinerStudioService = new LocalMinerStudioService();
        private readonly ServerMinerStudioService _serverMinerStudioService = new ServerMinerStudioService();

        private MinerStudioService() { }

        private IMinerStudioService service {
            get {
                if (RpcRoot.IsOuterNet) {
                    return _serverMinerStudioService;
                }
                else {
                    return LocalMinerStudioService;
                }
            }
        }

        public void QueryClientsAsync(QueryClientsRequest query) {
            service.QueryClientsAsync(query);
        }

        public void UpdateClientAsync(string objectId, string propertyName, object value, Action<ResponseBase, Exception> callback) {
            service.UpdateClientAsync(objectId, propertyName, value, callback);
        }

        public void UpdateClientsAsync(string propertyName, Dictionary<string, object> values, Action<ResponseBase, Exception> callback) {
            service.UpdateClientsAsync(propertyName, values, callback);
        }

        public void RemoveClientsAsync(List<string> objectIds, Action<ResponseBase, Exception> callback) {
            service.RemoveClientsAsync(objectIds, callback);
        }

        public void GetConsoleOutLinesAsync(IMinerData client, long afterTime) {
            service.GetConsoleOutLinesAsync(client, afterTime);
        }

        public void GetLocalMessagesAsync(IMinerData client, long afterTime) {
            service.GetLocalMessagesAsync(client, afterTime);
        }

        public void EnableRemoteDesktopAsync(IMinerData client) {
            service.EnableRemoteDesktopAsync(client);
        }

        public void BlockWAUAsync(IMinerData client) {
            service.BlockWAUAsync(client);
        }

        public void SwitchRadeonGpuAsync(IMinerData client, bool on) {
            service.SwitchRadeonGpuAsync(client, on);
        }

        public void RestartWindowsAsync(IMinerData client) {
            service.RestartWindowsAsync(client);
        }

        public void ShutdownWindowsAsync(IMinerData client) {
            service.ShutdownWindowsAsync(client);
        }

        public void SetAutoBootStartAsync(IMinerData client, SetAutoBootStartRequest request) {
            service.SetAutoBootStartAsync(client, request);
        }

        public void StartMineAsync(IMinerData client, Guid workId) {
            service.StartMineAsync(client, workId);
        }

        public void StopMineAsync(IMinerData client) {
            service.StopMineAsync(client);
        }

        public void UpgradeNTMinerAsync(IMinerData client, string ntminerFileName) {
            service.UpgradeNTMinerAsync(client, ntminerFileName);
        }

        public void GetDrivesAsync(IMinerData client) {
            service.GetDrivesAsync(client);
        }

        public void SetVirtualMemoryAsync(IMinerData client, Dictionary<string, int> data) {
            service.SetVirtualMemoryAsync(client, data);
        }

        public void GetLocalIpsAsync(IMinerData client) {
            service.GetLocalIpsAsync(client);
        }

        public void SetLocalIpsAsync(IMinerData client, List<LocalIpInput> data) {
            service.SetLocalIpsAsync(client, data);
        }

        public void GetOperationResultsAsync(IMinerData client, long afterTime) {
            service.GetOperationResultsAsync(client, afterTime);
        }

        public void GetSelfWorkLocalJsonAsync(IMinerData client) {
            service.GetSelfWorkLocalJsonAsync(client);
        }

        public void SaveSelfWorkLocalJsonAsync(IMinerData client, string localJson, string serverJson) {
            service.SaveSelfWorkLocalJsonAsync(client, localJson, serverJson);
        }

        public void GetGpuProfilesJsonAsync(IMinerData client) {
            service.GetGpuProfilesJsonAsync(client);
        }

        public void SaveGpuProfilesJsonAsync(IMinerData client, string json) {
            service.SaveGpuProfilesJsonAsync(client, json);
        }
    }
}
