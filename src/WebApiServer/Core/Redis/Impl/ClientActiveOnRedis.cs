﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NTMiner.Core.Redis.Impl {
    public class ClientActiveOnRedis : IClientActiveOnRedis {
        protected const string _redisKeyClientActiveOnById = RedisKeyword.ClientActiveOnById;

        protected readonly IMqRedis _redis;
        public ClientActiveOnRedis(IMqRedis redis) {
            _redis = redis;
        }

        public Task<Dictionary<string, DateTime>> GetAllAsync() {
            var db = _redis.RedisConn.GetDatabase();
            return db.HashGetAllAsync(_redisKeyClientActiveOnById).ContinueWith(t => {
                Dictionary<string, DateTime> list = new Dictionary<string, DateTime>();
                foreach (var item in t.Result) {
                    if (item.Value.HasValue) {
                        // 为了在redis-cli的易读性而存字符串
                        if (!DateTime.TryParse(item.Value, out DateTime activeOn)) {
                            activeOn = DateTime.MinValue;
                        }
                        list.Add(item.Name, activeOn);
                    }
                }
                return list;
            });
        }

        public Task SetAsync(string id, DateTime activeOn) {
            if (string.IsNullOrEmpty(id)) {
                return TaskEx.CompletedTask;
            }
            var db = _redis.RedisConn.GetDatabase();
            // 为了在redis-cli的易读性而存字符串
            return db.HashSetAsync(_redisKeyClientActiveOnById, id, activeOn.ToString("yyyy-MM-dd HH:mm:ss"));
        }

        public Task DeleteAsync(string id) {
            if (string.IsNullOrEmpty(id)) {
                return TaskEx.CompletedTask;
            }
            var db = _redis.RedisConn.GetDatabase();
            return db.HashDeleteAsync(_redisKeyClientActiveOnById, id);
        }
    }
}
